//
//  ViewController.h
//  AppDream(noborad)
//
//  Created by TheM on 3/20/56 BE.
//  Copyright (c) 2556 TheM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface ViewController : UIViewController<FBLoginViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;
@property (weak, nonatomic) IBOutlet UIButton *buttonLook;
@property (weak, nonatomic) IBOutlet UIView *pView;
@property (weak, nonatomic) IBOutlet FBLoginView *buttonLogin;

@property (weak, nonatomic) IBOutlet UIButton *loginFB;
@property (weak, nonatomic) IBOutlet UILabel *fontKun;

- (void) setImageProfile:(NSString *)fbID;

@end