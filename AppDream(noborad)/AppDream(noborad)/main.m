//
//  main.m
//  AppDream(noborad)
//
//  Created by TheM on 3/20/56 BE.
//  Copyright (c) 2556 TheM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
