//
//  ViewController.m
//  AppDream(noborad)
//
//  Created by TheM on 3/20/56 BE.
//  Copyright (c) 2556 TheM. All rights reserved.
//

#import "ViewController.h"
#import "logOutPage.h"
#import <QuartzCore/QuartzCore.h>
#import "AGMedallionView.h"
#import "AppDelegate.h"

@interface ViewController ()
//@property (strong, nonatomic) FBProfilePictureView *profilePictureView;

//- (void)sessionStateChanged:(NSNotification*)notification;
@end

@implementation ViewController
//@synthesize profilePictureView = _profilePictureView;

@synthesize logo = _logo;
@synthesize buttonLook = _buttonLook;
@synthesize buttonShare = _buttonShare;
@synthesize buttonLogin = _buttonLogin;
@synthesize loginFB = _loginFB;
@synthesize fontKun = _fontKun;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    

}

- (void) setImageProfile:(NSString *)fbID
{
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", fbID]]; //config url at load picture
            NSData *data = [NSData dataWithContentsOfURL:url]; //load data is picture then tranform is NSData
            UIImage *profilePic = [[UIImage alloc] initWithData:data]; //tranform NSData to UIImage
            AGMedallionView *medallionView = [[AGMedallionView alloc] init]; //class make circle
            medallionView.image = profilePic; // put facebook image to circle
            [self.pView addSubview:medallionView];
    
    //[ addSubview:medallionView]; // take circle in UIView

}

- (IBAction)loginFB:(id)sender {

    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    // If the user is authenticated, log out when the button is clicked.
    // If the user is not authenticated, log in when the button is clicked.
    if (FBSession.activeSession.isOpen) {
        [appDelegate closeSession];
   
        self.pView.hidden = YES;
        
        self.logo.hidden = NO;
        self.buttonShare.enabled = NO;
        self.buttonLook.enabled =NO;
    } else {
        // The user has initiated a login, so call the openSession method
        // and show the login UX if necessary.
        [appDelegate openSessionWithAllowLoginUI:YES];
        self.pView.hidden = YES;
        self.logo.hidden = YES;
        self.buttonShare.enabled = YES;
        self.buttonLook.enabled =YES;
        

        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.logo setImage:[UIImage imageNamed:@"Logoapp.png"]];

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(sessionStateChanged:)
     name:FBSessionStateChangedNotification
     object:nil];
    self.fontKun.font = [UIFont fontWithName:@"Kunlasatri-Bold" size:17];
//    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
   // [appDelegate openSessionWithAllowLoginUI:NO];

//    FBLoginView *buttonLogin = [[FBLoginView alloc] init];
//    buttonLogin.readPermissions = @[@"email", @"user_likes"];
//    
//    buttonLogin.publishPermissions = @[@"publish_actions"];
//    buttonLogin.defaultAudience = FBSessionDefaultAudienceFriends;
//
//    buttonLogin.frame = CGRectOffset(buttonLogin.frame,
//                                   (160 - (buttonLogin.frame.size.width / 2)),
//                                   240);
//    buttonLogin.delegate = self; //To see image user
//    
//   // [buttonLogin presentModalViewController:FBLoggingBehavior animated:NO];
//    [self.view addSubview:buttonLogin];
//  
//    //self.profilePictureView = [[FBProfilePictureView alloc] init];
//    //self.profilePictureView.frame = CGRectMake(90, 100, 150.0, 150.0);
//    [buttonLogin sizeToFit];
//   // [self.view addSubview:self.profilePictureView];
    
}




//- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
//                            user:(id<FBGraphUser>)user {
//    //self.profilePictureView.profileID = user.id;
//    //self.profilePictureView.hidden = NO;
//    self.pView.hidden = NO;
//    self.logo.hidden = YES;
//    self.buttonShare.enabled = YES;
//    self.buttonLook.enabled =YES;
//    
//    [[self navigationController] setNavigationBarHidden:YES animated:YES];//hidden navigation must take on top
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", user.id]]; //config url at load picture
//    NSData *data = [NSData dataWithContentsOfURL:url]; //load data is picture then tranform is NSData
//    UIImage *profilePic = [[UIImage alloc] initWithData:data]; //tranform NSData to UIImage
//    AGMedallionView *medallionView = [[AGMedallionView alloc] init]; //class make circle
//    medallionView.image = profilePic; // put facebook image to circle
//    [self.pView addSubview:medallionView]; // take circle in UIView
//
//    
//
//    
//    
//    NSLog(@"hello");
//}

//- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
//    //self.profilePictureView.profileID = nil;
//  
//    //self.profilePictureView.hidden =YES;
//    self.pView.hidden = YES;
//    self.logo.hidden = NO;
//    self.buttonShare.enabled = NO;
//    self.buttonLook.enabled =NO;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // Dispose of any resources that can be recreated.
    
}


- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        [self.loginFB setBackgroundImage:[UIImage imageNamed:@"Logout"] forState:UIControlStateNormal];
        self.pView.hidden = NO;
        
    } else {
        [self.loginFB setBackgroundImage:[UIImage imageNamed:@"Facebooklogin.png"] forState:UIControlStateNormal];
        self.pView.hidden = YES;
        
    } 
}



@end
