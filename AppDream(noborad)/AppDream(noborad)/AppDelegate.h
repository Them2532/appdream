//
//  AppDelegate.h
//  AppDream(noborad)
//
//  Created by TheM on 3/20/56 BE.
//  Copyright (c) 2556 TheM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

#import "ViewController.h"
@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>


- (void) closeSession;
extern NSString *const FBSessionStateChangedNotification;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *viewController;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@end
